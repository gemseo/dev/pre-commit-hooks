Credits
=======

The developers thank all the open source libraries making this possible.

### External Dependencies

The code depends on software with compatible licenses that are listed below.

[GitPython](https://gitpython.readthedocs.io): BSD-3-Clause

[Pytest](https://pytest.org): MIT

[Python](http://python.org/): Python Software License

### External applications

External applications used, but not linked with the application, for development.

[black](https://black.readthedocs.io): MIT

[flake8](https://flake8.pycqa.org): MIT

[pre-commit](https://pre-commit.com): MIT

[reorder-python-imports](https://github.com/asottile/reorder_python_imports): MIT

[setuptools](https://setuptools.readthedocs.io/): MIT
