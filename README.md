pre-commit-hooks
================

An out-of-the-box hooks for pre-commit.

See also: https://github.com/pre-commit/pre-commit

### Using pre-commit-hooks with pre-commit

Add this to your `.pre-commit-config.yaml`

```yaml
-   repo: https://gitlab.com/gemseo/dev/pre-commit-hooks
    rev: 0.1.0  # Use the ref you want to point at
    hooks:
    -   id: add-author-to-headers
```

### Hooks available

#### `add-author-to-headers`
Add a git commit author (or ) as a comment in the headers of the staged files.
The authors are written as a comment block at the end of the first comment (if any)
or at the top of the file (taking into account top shebang or coding lines).
The comment block starts by a line with a section name followed by one line per author.
With the default settings, a comment block may look like:

```python
# The last line of the first comment block.
#
# Contributors:
# - First Author
# - Second Author
```

You can configure this with the following commandline options:
  - `--section-name` - Name of the authors section, defaults to `Contributors:`.
  - `--comment-prefix` - Comment prefix for the section contents, defaults to `# `.
  - `--comment-start` - First comment line before the section contents, defaults to `""`.
  - `--comment-end` - Last comment line after the section contents, defaults to `""`.
 
For `.rst` files, you may use `--comment-prefix='   '` and `--comment-start=$'..\n'`.

Currently, it may not work well on shallow git clone unless `--depth=1`.
