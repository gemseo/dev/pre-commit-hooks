# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
from __future__ import annotations

from io import StringIO
from itertools import product
from pathlib import Path

import pytest
from git import Actor
from git import Repo
from pre_commit_hooks.add_author_to_headers import _add_authors
from pre_commit_hooks.add_author_to_headers import _get_authors
from pre_commit_hooks.add_author_to_headers import _get_new_authors
from pre_commit_hooks.add_author_to_headers import _get_section_info
from pre_commit_hooks.add_author_to_headers import _get_staged_files
from pre_commit_hooks.add_author_to_headers import _get_username
from pre_commit_hooks.add_author_to_headers import Authors
from pre_commit_hooks.add_author_to_headers import COMMENT_PREFIX
from pre_commit_hooks.add_author_to_headers import ENCODING_PREFIX
from pre_commit_hooks.add_author_to_headers import HEADER
from pre_commit_hooks.add_author_to_headers import LIST_PREFIX
from pre_commit_hooks.add_author_to_headers import main
from pre_commit_hooks.add_author_to_headers import SHEBANG_PREFIX

DATA_PATH = Path(__file__).parent / "data"

SECTION_HEADER = f"{COMMENT_PREFIX}{HEADER}"


@pytest.fixture
def repo(tmp_path, monkeypatch):
    monkeypatch.chdir(tmp_path)
    yield Repo.init()


def test_authors():
    a = Authors()
    first_item = " Xaeuioc Y "
    a.add(first_item)
    padding = ("", " ")
    eol = ("", "\n")
    for item in product(
        padding, "xX", "àâ", "éèêë", "ùûü", "ïî", "ô", "ç", " -_.~", "yY", padding, eol
    ):
        a.add("".join(item))
    assert list(a) == [first_item.strip()]
    assert list(Authors(a)) == list(a)
    assert len(Authors(a)) == 1


def test_get_username(repo):
    with repo.config_writer() as config:
        config.set_value("user", "name", "foo")
    assert _get_username() == "foo"


def test_get_username_error(repo, monkeypatch):
    def config_reader():
        raise RuntimeError()  # pragma: no cover

    monkeypatch.setattr(Repo, "config_reader", config_reader)

    msg = "Cannot find the username."
    with pytest.raises(RuntimeError, match=msg):
        _get_username()


def create_commits(repo, commit=True):
    authors = ["author1", "author2", "author3"]

    # Commit a new file.
    file_name = "file"
    Path(file_name).write_text("")
    author = Actor(authors[0], "")
    repo.index.add([file_name])
    if commit:
        repo.index.commit("bla", author=author)
    yield file_name, authors[0:1]

    # Commit a change with another author.
    author.name = authors[1]
    Path(file_name).write_text("x\n")
    repo.index.add([file_name])
    if commit:
        repo.index.commit("bla", author=author)
    yield file_name, authors[0:2]

    # Rename the file with yet another author.
    author.name = authors[2]
    file_renamed = "renamed_file"
    repo.index.move([file_name, file_renamed])
    repo.index.add([file_renamed])
    if commit:
        repo.index.commit("bla", author=author)
    yield file_renamed, authors


def test_get_authors(repo):
    commits = create_commits(repo)

    file_name, authors = next(commits)
    assert set(_get_authors(file_name)) == set(authors)

    file_name, authors = next(commits)
    assert set(_get_authors(file_name)) == set(authors)

    file_name, authors = next(commits)
    assert set(_get_authors(file_name)) == set(authors)


def test_get_staged_files(repo):
    commits = create_commits(repo, commit=False)

    file_name, _ = next(commits)
    assert set(_get_staged_files()) == {file_name}

    file_name, _ = next(commits)
    assert set(_get_staged_files()) == {file_name}

    file_name, _ = next(commits)
    assert set(_get_staged_files()) == {file_name}


def test_get_staged_files_error(tmp_path, monkeypatch):
    monkeypatch.chdir(tmp_path)

    msg = "Cannot find the staged files."
    with pytest.raises(RuntimeError, match=msg):
        _get_staged_files()


def text_to_lines(text: str, **format_kwargs: str) -> list[str]:
    return StringIO(text.format(**format_kwargs)).readlines()


parametrized_comments = pytest.mark.parametrize(
    "comment_prefix,comment_start,comment_end",
    (
        (COMMENT_PREFIX, "", ""),
        ("  ", "..\n", ""),
    ),
)

parametrized_first_line = pytest.mark.parametrize(
    "first_line",
    ("", f"{SHEBANG_PREFIX} bla\n", f"{ENCODING_PREFIX} bla\n"),
)


@pytest.mark.parametrize(
    "text,section_range",
    (
        ("", (0, 0)),
        (
            """

not a comment
""",
            (0, 0),
        ),
        (
            """
not a comment
""",
            (0, 0),
        ),
        (
            """
{comment_start}{comment_prefix}comment
""",
            (1, 1),
        ),
        (
            """
{comment_start}{comment_prefix}comment

""",
            (1, 1),
        ),
        (
            """
{comment_start}{comment_prefix}comment
not a comment
""",
            (1, 1),
        ),
        (
            """
{comment_start}{comment_prefix}comment
not a comment
{comment_start}{comment_prefix}comment
""",
            (1, 1),
        ),
        (
            """
{comment_start}{comment_prefix}comment

{comment_start}{comment_prefix}comment
""",
            (1, 1),
        ),
        (
            """

{comment_start}{comment_prefix}comment
""",
            (2, 2),
        ),
        (
            """
{comment_start}{comment_prefix}comment
{comment_prefix}{header}
""",
            (2, 2),
        ),
        (
            """
{comment_start}{comment_prefix}comment
{comment_prefix}{header}
not a comment
""",
            (2, 2),
        ),
        (
            """
{comment_start}{comment_prefix}comment
{comment_prefix}{header}
{comment_prefix}comment
""",
            (2, 3),
        ),
        (
            """
{comment_start}{comment_prefix}comment
{comment_prefix}{header}
{comment_prefix}comment
not a comment
""",
            (2, 3),
        ),
    ),
)
@parametrized_first_line
@parametrized_comments
def test_get_lines_range(
    text, section_range, first_line, comment_prefix, comment_start, comment_end
):
    text = text[1:]

    if "{comment_start}" in text and comment_start.endswith("\n"):
        has_comment_start = True
        section_range = (section_range[0] + 1, section_range[1] + 1)
    else:
        has_comment_start = False

    if first_line:
        # Remove the first line ending that is used just for clarity of the text but
        # shall not exist in reality.
        text = first_line + text
        section_range = (section_range[0] + 1, section_range[1] + 1)

    lines = text_to_lines(
        text,
        header=HEADER,
        comment_prefix=comment_prefix,
        comment_start=comment_start,
        comment_end=comment_end,
    )

    assert _get_section_info(
        HEADER, comment_prefix, comment_start, comment_end, lines
    ) == section_range + (has_comment_start,)


@pytest.mark.parametrize(
    "lines,authors,new_authors",
    (
        ([], Authors(), Authors()),
        ([], Authors({"author1"}), Authors({"author1"})),
        (["# - author1"], Authors(), Authors()),
        (["# - author1"], Authors({"author1"}), Authors()),
        (["# - author1"], Authors({"author2"}), Authors({"author2"})),
    ),
)
def test_get_new_authors(lines, authors, new_authors):
    assert _get_new_authors(lines, authors) == new_authors


def test_add_authors_false():
    assert not _add_authors(Authors(), HEADER, COMMENT_PREFIX, "", "", [])


@pytest.mark.parametrize(
    "authors,text,expected_text",
    (
        (
            Authors({"author1"}),
            "",
            """
{comment_start}{comment_prefix}{header}
{comment_prefix}{list_prefix}author1
""",
        ),
        (
            Authors({"author1"}),
            """

bla
""",
            """
{comment_start}{comment_prefix}{header}
{comment_prefix}{list_prefix}author1

bla
""",
        ),
        (
            Authors({"author1"}),
            """
{comment_start}{comment_prefix}bla
""",
            """
{comment_start}{comment_prefix}bla
{comment_prefix}
{comment_prefix}{header}
{comment_prefix}{list_prefix}author1
""",
        ),
        (
            Authors({"author1"}),
            """
{comment_prefix}{header}
{comment_prefix}{list_prefix}author0
""",
            """
{comment_prefix}{header}
{comment_prefix}{list_prefix}author0
{comment_prefix}{list_prefix}author1
""",
        ),
    ),
)
@parametrized_comments
def test_add_authors_true(
    authors, text, expected_text, comment_prefix, comment_start, comment_end
):
    lines = text_to_lines(
        text[1:],
        header=HEADER,
        list_prefix=LIST_PREFIX,
        comment_prefix=comment_prefix,
        comment_start=comment_start,
        comment_end=comment_end,
    )

    assert _add_authors(
        authors, HEADER, comment_prefix, comment_start, comment_end, lines
    )

    expected_lines = text_to_lines(
        expected_text[1:],
        header=HEADER,
        list_prefix=LIST_PREFIX,
        comment_prefix=comment_prefix,
        comment_start=comment_start,
        comment_end=comment_end,
    )

    assert lines == expected_lines


def test_add_authors_rst():
    text = """
..
  bla

  bla
"""

    lines = text_to_lines(text[1:])

    assert _add_authors(Authors({"author1"}), HEADER, "  ", "..\n", "", lines)

    expected_text = f"""
..
  bla

  bla
  
  {HEADER}
  {LIST_PREFIX}author1
"""  # noqa: W293

    expected_lines = text_to_lines(expected_text[1:])

    assert lines == expected_lines


def test_main_one_author(repo):
    with repo.config_writer() as config:
        config.set_value("user", "name", "author1")

    file_name = "file"
    Path(file_name).write_text("# bla\n")
    repo.index.add([file_name])
    expected_text = """
# bla
# 
# Contributors:
# - author1
"""  # noqa: W291

    assert main([file_name]) == 1
    assert Path(file_name).read_text() == expected_text.lstrip()


def test_main_all_authors(repo):
    for file_name_, _ in create_commits(repo):
        file_name = file_name_
    assert main([file_name]) == 1
    assert (
        Path(file_name).read_text()
        == (DATA_PATH / "expected_file_all_authors").read_text()
    )
    assert main([file_name]) == 0
