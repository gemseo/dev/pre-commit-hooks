# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
from __future__ import annotations

import argparse
import typing
from collections import abc

if typing.TYPE_CHECKING:
    from typing import Final  # pragma: no cover
from typing import Iterable
from typing import Iterator
from typing import Sequence
from typing import MutableSequence

from git import Repo
from git.exc import GitCommandError

COMMENT_PREFIX = "# "
HEADER = "Contributors:"
SHEBANG_PREFIX = "#!"
ENCODING_PREFIX = "# -*- coding"
LIST_PREFIX = "- "
LINE_STOP_SENTINEL = -1


class Authors(abc.MutableSet):
    """A set like container where unicity is verified on simplified string."""

    __TRANSLATION_TABLE: Final = str.maketrans(
        "àâéèêëùûüïîôç", "aaeeeeuuuiioc", " -_.~"
    )

    def __init__(self, authors: Iterable[str] | None = None) -> None:
        self.__authors: dict[str, str] = {}
        if authors is not None:
            for author in authors:
                self.add(author)

    @classmethod
    def __reduce(cls, author: str) -> str:
        return str(author).lower().translate(cls.__TRANSLATION_TABLE)

    @staticmethod
    def __clean(author: str) -> str:
        return author.strip()

    def add(self, author: str) -> None:
        author = self.__clean(author)
        reduced_author = self.__reduce(author)
        if reduced_author not in self.__authors:
            self.__authors[reduced_author] = author

    def discard(self, author: str) -> None:
        self.__authors.pop(self.__reduce(self.__clean(author)), None)

    def __contains__(self, author: object) -> bool:
        assert isinstance(author, str)
        return self.__reduce(self.__clean(author)) in self.__authors

    def __iter__(self) -> Iterator[str]:
        return iter(self.__authors.values())

    def __len__(self) -> int:
        return len(self.__authors)


def _get_section_info(
    header: str,
    comment_prefix: str,
    comment_start: str,
    comment_end: str,
    lines: Sequence[str],
) -> tuple[int, int, bool]:
    if not lines:
        return 0, 0, False

    # Skip special lines.
    if lines[0].startswith(SHEBANG_PREFIX) or lines[0].startswith(ENCODING_PREFIX):
        first_line_id = 1
    else:
        first_line_id = 0

    # Find where the first comment block starts.
    has_comment_start = False
    for line_id, line in enumerate(lines[first_line_id:], start=first_line_id):
        if not line.strip():
            continue
        if comment_start and line.startswith(comment_start):
            has_comment_start = True
            first_line_id = line_id + 1
        elif line.startswith(comment_prefix):
            first_line_id = line_id
        break

    lines_start_id = first_line_id
    lines_stop_id = LINE_STOP_SENTINEL

    # Find where, in the first comment if any, is the start and end of the authors
    # section.
    for line_id, line in enumerate(lines[first_line_id:], start=first_line_id):
        if line.startswith(f"{comment_prefix}{header}"):
            lines_start_id = line_id + 1
        elif not line.startswith(comment_prefix):
            if lines_stop_id == LINE_STOP_SENTINEL:
                lines_stop_id = line_id
            # For .rst files, a comment block may contain an empty line for which we
            # shall not break yet.
            if line.strip() != comment_prefix.strip():
                break
        elif lines_stop_id != LINE_STOP_SENTINEL:
            # Reset the line stop search because the comment block continues.
            lines_stop_id = LINE_STOP_SENTINEL
    else:
        if lines_stop_id == LINE_STOP_SENTINEL:
            # The last line is a comment, stop is the next line.
            lines_stop_id = len(lines)

    if lines_start_id == first_line_id:
        # No section header was found.
        lines_start_id = lines_stop_id

    return lines_start_id, lines_stop_id, has_comment_start


def _get_new_authors(lines: Iterable[str], authors: Authors) -> Authors:
    if not lines:
        return authors

    new_authors = Authors(authors)

    for line in lines:
        author = line.split(LIST_PREFIX)[-1]
        if author in authors:
            new_authors.discard(author)

    return new_authors


def _add_authors(
    authors: Authors,
    section_name: str,
    comment_prefix: str,
    comment_start: str,
    comment_end: str,
    lines: MutableSequence[str],
) -> Authors:
    section_header = f"{comment_prefix}{section_name}"

    lines_start, lines_stop, has_comment_start = _get_section_info(
        section_name, comment_prefix, comment_start, comment_end, lines
    )

    new_authors = _get_new_authors(lines[lines_start:lines_stop], authors)

    if not new_authors:
        return Authors()

    section_is_missing = lines_start == lines_stop

    if (
        section_is_missing
        and comment_end
        and not lines[lines_stop].startswith(comment_end)
    ):
        lines.insert(lines_stop, f"{comment_end}\n")

    for author in new_authors:
        lines.insert(lines_stop, f"{comment_prefix}{LIST_PREFIX}{author}\n")


    if section_is_missing:
        lines.insert(lines_stop, f"{section_header}\n")
        if comment_start and not has_comment_start:
            lines.insert(lines_stop, comment_start)
        if lines_stop != 0 and lines[lines_stop - 1].startswith(comment_prefix):
            # Add an empty commented line before the header if there was already a
            # commented line before.
            lines.insert(lines_stop, f"{comment_prefix}\n")

    return new_authors


def _get_username() -> str:
    """Return the git username."""
    try:
        name = Repo().config_reader().get_value("user", "name")
    except BaseException:
        raise RuntimeError("Cannot find the username.")
    return name


def _get_authors(file_path: str) -> Authors:
    """Return the authors of a file."""
    repo = Repo()

    if not repo.head.is_valid():
        # The current branch is empty.
        return Authors()

    log = repo.git.log("--format=format:%an", "--follow", file_path)

    authors = Authors()
    for line in log.splitlines():
        authors.add(line)

    return authors


def _get_staged_files() -> tuple[str]:
    """Return the staged files."""
    try:
        diff = Repo().git.diff("--name-only", "--cached", "--diff-filter=AM")
    except BaseException:
        raise RuntimeError("Cannot find the staged files.")
    return tuple(diff.splitlines())


def main(argv: Sequence[str] | None = None) -> int:
    """Main entry point."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--section-name",
        type=str,
        default=f"{HEADER}",
        help="Name of the authors section",
    )
    parser.add_argument(
        "--comment-prefix",
        type=str,
        default=f"{COMMENT_PREFIX}",
        help="Comment prefix for the section contents",
    )
    parser.add_argument(
        "--comment-start",
        type=str,
        default="",
        help="First comment line before the section contents",
    )
    parser.add_argument(
        "--comment-end",
        type=str,
        default="",
        help="Last comment line after the section contents",
    )
    parser.add_argument("file_paths", nargs="*", help="File paths to process")
    args = parser.parse_args(argv)

    staged_files = _get_staged_files()

    if staged_files:
        username = _get_username()

    return_code = 0

    for file_path in args.file_paths:
        authors = _get_authors(file_path)
        if file_path in staged_files:
            authors.add(username)

        with open(file_path) as file_stream:
            lines = file_stream.readlines()

        new_authors = _add_authors(
            authors,
            args.section_name,
            args.comment_prefix,
            args.comment_start,
            args.comment_end,
            lines,
        )

        if new_authors:
            with open(file_path, mode="w") as file_stream:
                file_stream.writelines(lines)
            return_code = 1
            print(f"Added author(s) to {file_path}: {', '.join(new_authors)}")

    return return_code


if __name__ == "__main__":
    raise SystemExit(main())
